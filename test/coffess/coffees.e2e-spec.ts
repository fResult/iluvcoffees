import { UpdateCoffeeDto } from "./../../src/coffees/dto/update-coffee.dto";
import { TypeOrmModule } from "@nestjs/typeorm";
import { NestApplication } from "@nestjs/core";
import { HttpServer, HttpStatus, ValidationPipe } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import { CoffeesModule } from "../../src/coffees/coffees.module";
import { CreateCoffeeDto } from "./../../src/coffees/dto/create-coffee.dto";
import * as request from "supertest";

describe("[Feature] Coffees - /coffees", () => {
  const coffee = {
    name: "Shipwreck Roast",
    brand: "Buddy Brew",
    flavors: ["Chocolate", "Vanilla"],
  };
  const expectedPartialCoffee = expect.objectContaining({
    ...coffee,
    flavors: expect.arrayContaining(
      coffee.flavors.map((name) => expect.objectContaining({ name })),
    ),
  });
  let app: NestApplication;
  let httpServer: HttpServer;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        CoffeesModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => ({
            type: "postgres",
            host: "localhost",
            port: 5433,
            username: "postgres",
            password: "pass123",
            database: "postgres",
            autoLoadEntities: true,
            synchronize: true,
          }),
        }),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
        whitelist: true,
        forbidNonWhitelisted: true,
        transformOptions: {
          enableImplicitConversion: true,
        },
      }),
    );

    await app.init();
    httpServer = app.getHttpServer();
  });

  it("Create [POST /]", async () => {
    return request(httpServer)
      .post("/coffees")
      .send(coffee as unknown as CreateCoffeeDto)
      .expect(HttpStatus.CREATED)
      .then(({ body: respBody }) => {
        expect(respBody).toEqual({
          created_coffee: expectedPartialCoffee,
        });
      });
  });

  it("Get all [GET /]", async () => {
    return request(httpServer)
      .get("/coffees")
      .then(({ body: respBody }) => {
        expect(respBody.coffees.length).toBeGreaterThan(0);
        expect(respBody.coffees[0]).toEqual(expectedPartialCoffee);
      });
  });

  // it.todo("Get one [GET /:id]");
  it("Get one [GET /:id]", async () => {
    return request(httpServer)
      .get("/coffees/1")
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialCoffee);
      });
  });

  it("Update one [PATCH /:id]", async () => {
    const updatedCoffeeDTO: UpdateCoffeeDto = {
      ...coffee,
      // brand: "NesCafe",
      brand: "Buddy Brew",
    } as unknown as UpdateCoffeeDto;

    return request(httpServer)
      .patch("/coffees/1")
      .send(updatedCoffeeDTO)
      .then(({ body: respBody }) => {
        expect(respBody.updated_coffee.brand).toEqual(updatedCoffeeDTO.brand);
      });
  });

  it("Delete one [DELETE /:id]", () => {
    const coffeeID = 4;
    return request(httpServer)
      .delete(`/coffees/${coffeeID}`)
      .expect(HttpStatus.OK)
      .then(() => {
        return request(httpServer)
          .get(`/coffees/${coffeeID}`)
          .expect(HttpStatus.NOT_FOUND);
      });
  });

  afterAll(async () => {
    await app?.close?.();
  });
});
