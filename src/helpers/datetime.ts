const options: Intl.DateTimeFormatOptions = {
  day: "2-digit",
  month: "short",
  year: "numeric",
  hour: "2-digit",
  minute: "2-digit",
  second: "2-digit",
  timeZone: "Asia/Bangkok",
  hour12: true,
};

export function formatDatetime(
  datetime: Date = new Date(),
  intlOptions: Intl.DateTimeFormatOptions = options,
): string {
  return Intl.DateTimeFormat("en-US", {
    ...intlOptions,
    ...options,
  }).format(datetime);
}
