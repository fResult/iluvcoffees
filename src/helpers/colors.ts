const colors = [1, 2, 4, 7, 6, 5, 3, 0].map(
  (x) => (str: string) => `\x1b[3${x}m${str}\x1b[0m`,
);
export const red = colors[0];
export const green = colors[1];
export const blue = colors[2];
export const white = colors[3];
export const cyan = colors[4];
export const magenta = colors[5];
export const yellow = colors[6];
export const black = colors[7];
