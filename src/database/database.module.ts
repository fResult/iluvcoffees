import { DynamicModule, Module } from "@nestjs/common";
import { createConnection, ConnectionOptions } from "typeorm";

@Module({})
export class DatabaseModule {
  static register(connOptions: ConnectionOptions): DynamicModule {
    return {
      module: DatabaseModule,
      providers: [
        {
          provide: "CONNECTION",
          useValue: createConnection(connOptions),
        },
      ],
    };
  }
}
