import { MigrationInterface, QueryRunner } from "typeorm";

export class CoffeeRefactor1641205813400 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.manager.query(
      "ALTER TABLE coffees RENAME COLUMN name TO title",
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.manager.query(
      "ALTER TABLE coffees RENAME COLUMN title TO name",
    );
  }
}
