import { Module } from "@nestjs/common";
import { ConnectionOptions } from "typeorm";

import { DatabaseModule } from "./../database/database.module";
import { CoffeesModule } from "../coffees/coffees.module";

import { CoffeeRatingService } from "./coffee-rating.service";

// May use another database
const anotherDBConfig: ConnectionOptions = {
  type: "postgres",
  host: "another-server",
  username: "admin",
  password: "password007",
};

@Module({
  imports: [DatabaseModule.register(anotherDBConfig), CoffeesModule],
  providers: [CoffeeRatingService],
})
export class CoffeeRatingModule {}
