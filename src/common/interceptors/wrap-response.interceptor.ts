import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from "@nestjs/common";
import { map } from "rxjs";

@Injectable()
export class WrapResponseInterceptor implements NestInterceptor {
  async intercept(ctx: ExecutionContext, next: CallHandler): Promise<any> {
    return next.handle().pipe(map((data) => ({ data })));
  }
}
