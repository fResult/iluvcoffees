import { LoggingMiddleware } from "./middleware/logging.middleware";
import { ConfigModule } from "@nestjs/config";
import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { APP_GUARD } from "@nestjs/core";
import { ApiKeyGuard } from "./guards/api-key.guard";

@Module({
  imports: [ConfigModule],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ApiKeyGuard,
    },
  ],
})
export class CommonModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // ONLY ROUTES /coffees/<any> and HTTP method GET
    // consumer
    // .apply(LoggingMiddleware)
    // .forRoutes({ method: RequestMethod.GET, path: "/coffees/*" });

    // Exclude only ROUTE /coffees/<any>
    // consumer.apply(LoggingMiddleware).exclude("/coffees/*").forRoutes("*");

    // ALL ROUTES
    consumer.apply(LoggingMiddleware).forRoutes("*");
  }
}
