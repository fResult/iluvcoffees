import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from "@nestjs/common";
import { Response } from "express";
import { formatDatetime } from "../../helpers/datetime";

// Ref: https://docs.nestjs.com/exception-filters
@Catch(HttpException)
export class HttpExceptionFilter<T extends HttpException>
  implements ExceptionFilter
{
  catch(exception: T, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const resp = ctx.getResponse<Response>();

    const status = exception.getStatus();
    const exceptionResp = exception.getResponse();
    const error =
      typeof resp === "string"
        ? { message: exceptionResp }
        : (exceptionResp as object);

    const humanReadableDateTime = formatDatetime();

    console.log("test", error, HttpExceptionFilter.name);
    resp.status(status).json({
      ...error,
      timestamp: humanReadableDateTime,
    });
  }
}
