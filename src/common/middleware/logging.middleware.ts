import { Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request } from "express";

@Injectable()
export class LoggingMiddleware implements NestMiddleware {
  use(req: Request, res: any, next: NextFunction) {
    console.time("Request-response time");
    console.log("Hi, from middleware");

    res.on("finish", () => {
      console.timeEnd("Request-response time");
    });
    next();
  }
}
