import { CoffeesController } from "./../../coffees/coffees.controller";
import { ConfigService } from "@nestjs/config";
import { IS_PUBLIC_KEY } from "./../decorators/public.decorator";
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Request } from "express";
import { Observable } from "rxjs";

@Injectable()
export class ApiKeyGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly configService: ConfigService,
  ) {}

  canActivate(
    ctx: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // Use `ctx.getClass()` when we need to retrieve metadata from *class level*
    const isPublic = this.reflector.get(IS_PUBLIC_KEY, ctx.getHandler());
    if (isPublic) return true;

    const req = ctx.switchToHttp().getRequest<Request>();
    const authHeader = req.headers.authorization;

    //return req.header("Authorization") === process.env.API_KEY
    return authHeader === this.configService.get("API_KEY");
  }
}
