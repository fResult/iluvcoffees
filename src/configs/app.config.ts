import { ConfigFactory } from "@nestjs/config";
import coffeesConfig, {
  ICoffeesConfig,
} from "../coffees/configs/coffees.config";

export interface IAppConfig {
  environment: string;
  database: {
    host: string;
    port: number;
  };
  coffees?: ICoffeesConfig;
}

console.log("conf", coffeesConfig());
/**
 * Ref: https://docs.nestjs.com/techniques/configuration#custom-configuration-files
 */
const appConfig: ConfigFactory<IAppConfig> = () => ({
  environment: process.env.NODE_ENV || "development",
  database: {
    host: process.env.DATABASE_HOST || "localhost",
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
  },
  coffees: coffeesConfig() as ICoffeesConfig,
});

export default appConfig;
