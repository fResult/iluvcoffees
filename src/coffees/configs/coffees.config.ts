import { registerAs } from "@nestjs/config";

export interface ICoffeesConfig {
  foo: "bar" | "baz";
}

export default registerAs<ICoffeesConfig>("coffees", () => {
  return { foo: "bar" };
});
