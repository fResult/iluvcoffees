import { Event } from "../events/entities";
import { Inject, Injectable, NotFoundException } from "@nestjs/common";
import { ConfigService, ConfigType } from "@nestjs/config";
import { InjectRepository } from "@nestjs/typeorm";
import coffeesConfig, {
  ICoffeesConfig,
} from "../coffees/configs/coffees.config";
import { Connection, Like, Repository } from "typeorm";
import { PaginationQueryDto } from "./../common/dto/pagination-query.dto";
import { CreateCoffeeDto, UpdateCoffeeDto } from "./dto";
import { Coffee, Flavor } from "./entities";

@Injectable()
export class CoffeesService {
  constructor(
    /** Ref: https://docs.nestjs.com/techniques/configuration#using-the-configservice */
    /*private readonly configService: ConfigService,
    @Inject(coffeesConfig.KEY)
    private readonly coffeesConfiguration: ConfigType<() => ICoffeesConfig>,*/
    @InjectRepository(Coffee)
    private readonly coffeeRepository: Repository<Coffee>,
    @InjectRepository(Flavor)
    private readonly flavorRepository: Repository<Flavor>,
    private readonly connection: Connection,
  ) {
    /*const coffeesConfig = this.configService.get<ICoffeesConfig>("coffees.foo");
    console.log("coffeesConfig", coffeesConfig);
    console.log(
      // "[best practice] coffeesConfig:",
      this.coffeesConfiguration.foo,
    );
    */
  }

  findAll({ offset, limit }: PaginationQueryDto): Promise<Coffee[]> {
    return this.coffeeRepository.find({
      skip: offset,
      take: limit,
      // where: {
      //   name: Like(`%${param.name}%`),
      // },
      relations: ["flavors"],
    });
  }

  async findOne(id: number): Promise<Coffee> {
    const coffee = await this.coffeeRepository.findOne(id, {
      relations: ["flavors"],
    });
    if (!coffee) {
      throw new NotFoundException(`Coffee ID #${id} not found`);
    }
    // await this.recommendCoffee(coffee);
    return coffee;
  }

  async preloadFlavorByName(name: string): Promise<Flavor> {
    const existingFlavor = await this.flavorRepository.findOne({ name });
    return existingFlavor || this.flavorRepository.create({ name });
  }

  async create(createCoffeeDTO: CreateCoffeeDto) {
    const flavors = await Promise.all(
      createCoffeeDTO.flavors.map(this.preloadFlavorByName.bind(this)),
    );

    const createdCoffee = this.coffeeRepository.create({
      ...createCoffeeDTO,
      flavors,
    });

    return this.coffeeRepository.save(createdCoffee);
  }

  async update(id: string, updateCoffeeDTO: UpdateCoffeeDto): Promise<Coffee> {
    const flavors =
      updateCoffeeDTO.flavors &&
      (await Promise.all(
        updateCoffeeDTO.flavors.map(this.preloadFlavorByName.bind(this)),
      ));

    const updatedCoffee = await this.coffeeRepository.preload({
      id: Number(id),
      ...updateCoffeeDTO,
      flavors,
    });
    if (!updatedCoffee) {
      throw new NotFoundException(`Coffee ID #${id} not found`);
    }

    return this.coffeeRepository.save({ ...updatedCoffee, flavors });
  }

  async remove(id: number): Promise<Coffee> {
    const coffee = await this.findOne(id);
    return this.coffeeRepository.remove(coffee);
  }

  async recommendCoffee(coffee: Coffee) {
    const queryRunner = this.connection.createQueryRunner();
    queryRunner.connect();
    queryRunner.startTransaction();

    try {
      coffee.recommendations++;

      const recommendEvent = new Event();
      recommendEvent.name = "recommend_coffee";
      recommendEvent.type = "coffee";
      recommendEvent.payload = { coffeeID: coffee.id };

      await queryRunner.manager.save(coffee);
      await queryRunner.manager.save(recommendEvent);
    } catch (err) {
      console.error({ message: err });
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }
  }
}
