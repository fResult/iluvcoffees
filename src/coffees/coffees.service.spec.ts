import { NotFoundException } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { Test, TestingModule } from "@nestjs/testing";
import { Flavor } from "./entities/flavor.entity";
import { Coffee } from "./entities/coffee.entity";
import { CoffeesService } from "./coffees.service";
import { getRepositoryToken } from "@nestjs/typeorm";

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
type MockRepositoryFactory = <T>() => MockRepository<T>;

const createMockRepository: MockRepositoryFactory = <T>() => ({
  findOne: jest.fn<T, string[]>(),
  create: jest.fn<T, string[]>(),
});

describe("CoffeesService", () => {
  let service: CoffeesService;
  let coffeesRepo: MockRepository<Coffee>;
  let flavorsRepo: MockRepository<Flavor>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CoffeesService,
        { provide: Connection, useValue: {} },
        {
          provide: getRepositoryToken(Flavor),
          useValue: createMockRepository<Flavor>(),
        },
        {
          provide: getRepositoryToken(Coffee),
          useValue: createMockRepository<Coffee>(),
        },
      ],
    }).compile();

    // For `transient` or `request` scope
    // service = await module.resolve<CoffeesService>(CoffeesService);
    service = module.get<CoffeesService>(CoffeesService);

    coffeesRepo = module.get<MockRepository<Coffee>>(
      getRepositoryToken(Coffee),
    );
    flavorsRepo = module.get<MockRepository<Flavor>>(
      getRepositoryToken(Flavor),
    );
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("findOne", () => {
    describe("when coffee with ID exist", () => {
      it("should return the coffee object", async () => {
        const coffeeID = 1;
        const expectedCoffee = {};

        coffeesRepo.findOne.mockReturnValue(expectedCoffee);
        const coffee = await service.findOne(coffeeID);
        expect(coffee).toEqual(expectedCoffee);
      });
    });

    describe("otherwise", () => {
      it("should throw the `NotFoundException`", async () => {
        const coffeeID = 123948;
        // jest.spyOn(coffeesRepo, "findOne").mockReturnValue(undefined);
        coffeesRepo.findOne.mockReturnValue(undefined);
        try {
          await service.findOne(coffeeID);
        } catch (err: unknown) {
          expect(err).toBeInstanceOf(NotFoundException);
          if (err instanceof Error) {
            expect(err.message).toEqual(`Coffee ID #${coffeeID} not found`);
          }
        }
      });
    });
  });
});
