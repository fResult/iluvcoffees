import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";

import { CoffeesController } from "./coffees.controller";
import { CoffeesService } from "./coffees.service";

import { Event } from "../events/entities";
import { Coffee, Flavor } from "./entities";

import { COFFEE_BRANDS } from "./coffees.constants";

import coffeesConfig from "./configs/coffees.config";

@Module({
  imports: [
    TypeOrmModule.forFeature([Coffee, Flavor, Event]),
    ConfigModule.forFeature(coffeesConfig),
  ],
  controllers: [CoffeesController],
  providers: [
    CoffeesService,
    {
      provide: COFFEE_BRANDS,
      useFactory: () => ["buddy brew", "nescafe"],
    },
  ],
  exports: [CoffeesService],
})
export class CoffeesModule {}
