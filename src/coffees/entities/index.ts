import { Flavor } from "./flavor.entity";
import { Coffee } from "./coffee.entity";

export { Coffee, Flavor };
