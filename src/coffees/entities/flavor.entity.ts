import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

import { Coffee } from ".";

@Entity({ name: "flavors" })
export class Flavor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToMany(() => Coffee, (coffee) => coffee.flavors)
  coffees: Coffee[];
}
