import { ApiForbiddenResponse, ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { Flavor } from "../entities";

@ApiForbiddenResponse({ description: "Forbidden." })
export class CreateCoffeeDto {
  @ApiProperty({ description: "The name of coffee", example: "Expresso" })
  @IsString()
  readonly name: string;

  @ApiProperty({ description: "The brand", example: "Nespresso" })
  @IsString()
  readonly brand: string;

  @ApiProperty({
    description: "The flavor for this coffee",
    example: ["Chocolate", "Milk", "Vanilla"],
  })
  @IsString({ each: true })
  readonly flavors: Flavor[];
}
