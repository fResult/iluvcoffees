import { CreateCoffeeDto } from "./create-coffee.dto";
import { UpdateCoffeeDto } from "./update-coffee.dto";

export { CreateCoffeeDto, UpdateCoffeeDto };
