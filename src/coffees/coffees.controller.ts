import { Request } from "express";
import { REQUEST } from "@nestjs/core";
import { REQUEST_CONTEXT_ID } from "@nestjs/core/router/request/request-constants";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from "@nestjs/common";
import { ApiCreatedResponse, ApiResponse, ApiTags } from "@nestjs/swagger";

import { CreateCoffeeDto, UpdateCoffeeDto } from "./dto";
import { PaginationQueryDto } from "../common/dto/pagination-query.dto";
import { Coffee } from "./entities/coffee.entity";
import { CoffeesService } from "./coffees.service";

import { red } from "./../helpers/colors";
import { Public } from "../common/decorators/public.decorator";
import { ParseIntPipe } from "../common/pipes/parse-int.pipe";
import { Protocol } from "../common/decorators/protocol.decorator";

// Building Block: Controller Scope
// @UsePipes(ValidationPipe)
@ApiTags("coffees")
@Controller("coffees")
export class CoffeesController {
  constructor(
    // @Inject(REQUEST) private request: Request,
    private readonly coffeesService: CoffeesService,
  ) {
    console.log(
      `${red(
        "CoffeesController is initiated",
      )} ${REQUEST_CONTEXT_ID.toString()}`,
    );
  }

  // Building Block: Method Scope
  @UsePipes(ValidationPipe)
  @ApiResponse({
    status: 403,
    description: "Forbidden.",
    links: { Google: { $ref: "https://google.com" } },
  })
  @ApiResponse({
    status: 200,
    description: "OK.",
    content: {
      "application/json": {
        example: JSON.parse(`{
          "coffees": [
            {"id": 18,"name": "Coffee #1","description": "Coffee is good.","brand": "Brand #1","recommendations": 0,"flavors": [{"id": 1,"name": "Chocolate"},{"id": 3,"name": "Vanilla"}]},
            {"id": 19,"name": "Coffee #2","description": null,"brand": "Brand #1","recommendations": 0,"flavors": [{"id": 1,"name": "Chocolate"},{"id": 2,"name": "Milk"}]}
          ]
        }`),
      },
    },
  })
  @Public()
  @Get()
  async findAll(
    @Query() paginationQuery: PaginationQueryDto,
  ): Promise<PaginationQueryDto & { coffees: Coffee[] }> {
    const { limit = 10, offset = 0 } = paginationQuery;

    const coffees = await this.coffeesService.findAll({ offset, limit });
    return { coffees: coffees, limit, offset };
  }

  @Get("/:id")
  async findOne(@Param("id", ParseIntPipe) id: number): Promise<Coffee> {
    return this.coffeesService.findOne(id);
  }

  @ApiCreatedResponse({
    description: "Created.",
    content: {
      "application/json": {
        example: JSON.parse(`{
          "created_coffee": {
            "id": 1,
            "name": "Espresso",
            "brand": "Nespresso",
            "flavors": [{"id":1,"name":"Chocolate"},{"id":2,"name":"Milk"},{"id":3,"name":"Vanilla"}]
          }
        }`),
      },
    },
  })
  @Post()
  async create(@Body() createCoffeeDto: CreateCoffeeDto): Promise<{
    created_coffee: CreateCoffeeDto;
  }> {
    const createdCoffee = await this.coffeesService.create(createCoffeeDto);
    return { created_coffee: createdCoffee };
  }

  @Patch(":id")
  async update(
    @Param("id") id: string,
    @Body(/* Building Block */ ValidationPipe)
    updateCoffeeDto: UpdateCoffeeDto,
  ) {
    const updatedCoffee = await this.coffeesService.update(id, updateCoffeeDto);
    return { updated_coffee: updatedCoffee };
  }

  @Delete(":id")
  async remove(
    @Param("id", /* Building Block */ ValidationPipe)
    id: number,
  ) {
    const deletedCoffee = await this.coffeesService.remove(id);
    return { deleted_coffee: deletedCoffee };
  }
}
